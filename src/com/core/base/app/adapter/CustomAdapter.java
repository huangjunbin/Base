package com.core.base.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.core.base.R;
import com.core.base.app.ui.MaterialActivity;
import com.core.base.widget.material.Button;
import com.core.base.widget.material.ButtonIcon;
import com.core.base.widget.material.ButtonRectangle;

public class CustomAdapter extends
		RecyclerView.Adapter<CustomAdapter.ViewHolder> {
	private final ArrayList<String> textArrayList;
	private Context mContext;

	public CustomAdapter(Context context) {
		this.textArrayList = new ArrayList<String>();
		mContext = context;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View v = LayoutInflater.from(viewGroup.getContext()).inflate(
				R.layout.row_view, null);

		return new ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, final int i) {
		viewHolder.textView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mContext.startActivity(new Intent(mContext,
						MaterialActivity.class));
			}
		});

		viewHolder.textView.getTextView().setText((textArrayList.get(i)));
	}

	@Override
	public int getItemCount() {
		return textArrayList.size();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		ButtonRectangle textView;

		public ViewHolder(View v) {
			super(v);
			textView = (ButtonRectangle) v.findViewById(R.id.text);
		}
	}

	public void updateList(ArrayList<String> stringArrayList) {
		this.textArrayList.clear();
		this.textArrayList.addAll(stringArrayList);
		this.notifyDataSetChanged();
	}

}
