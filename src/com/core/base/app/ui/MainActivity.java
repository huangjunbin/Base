package com.core.base.app.ui;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;

import com.core.base.R;
import com.core.base.annocation.InjectResource;
import com.core.base.annocation.InjectView;
import com.core.base.app.adapter.CustomAdapter;
import com.core.base.widget.HeaderBar;
import com.core.base.widget.SwipeRefreshLayout;
import com.core.base.widget.SwipeRefreshLayout.OnLoadListener;
import com.core.base.widget.SwipeRefreshLayout.OnRefreshListener;
import com.core.base.widget.material.RevealColorView;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements OnClickListener,
        OnRefreshListener, OnLoadListener {

    @InjectView(R.id.reveal)
    private RevealColorView revealColorView;


    @InjectView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeLayout;
    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.header)
    private HeaderBar bar;

    @InjectResource(R.string.app_name)
    private String name;


    private View selectedView;
    private int backgroundColor;
    private ArrayList<String> sourcesArrayList = new ArrayList<String>();

    public MainActivity() {
        super(R.layout.activity_main);

    }

    @Override
    public void initViews() {

        setSupportActionBar(toolbar);

        backgroundColor = Color.parseColor("#212121");
        bar.setTitle(name);
        findViewById(R.id.btn_1).setOnClickListener(this);
        findViewById(R.id.btn_2).setOnClickListener(this);
        findViewById(R.id.btn_3).setOnClickListener(this);
        findViewById(R.id.btn_4).setOnClickListener(this);

    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void initData() {

        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setOnLoadListener(this);
        mSwipeLayout.setColor(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mSwipeLayout.setMode(SwipeRefreshLayout.Mode.BOTH);
        mSwipeLayout.setLoadNoFull(false);

    }

    @Override
    public void bindViews() {
        sourcesArrayList.add("Samsung");
        sourcesArrayList.add("Android");
        sourcesArrayList.add("Google");
        sourcesArrayList.add("Asus");
        sourcesArrayList.add("Apple");
        sourcesArrayList.add("Samsung");
        sourcesArrayList.add("Android");
        sourcesArrayList.add("Google");
        sourcesArrayList.add("Asus");
        sourcesArrayList.add("Apple");
        sourcesArrayList.add("Samsung");
        sourcesArrayList.add("Android");
        sourcesArrayList.add("Google");
        sourcesArrayList.add("Asus");
        sourcesArrayList.add("Apple");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        CustomAdapter customAdapter = new CustomAdapter(this);
        customAdapter.updateList(sourcesArrayList);
        recyclerView.setAdapter(customAdapter);
    }

    @Override
    public void onClick(View v) {
        final int color = getColor(v);
        final Point p = getLocationInView(revealColorView, v);

        if (selectedView == v) {
            revealColorView.hide(p.x, p.y, backgroundColor, 0, 300, null);
            selectedView = null;
        } else {
            revealColorView.reveal(p.x, p.y, color, v.getHeight() / 2, 340,
                    null);
            selectedView = v;
        }

    }

    private Point getLocationInView(View src, View target) {
        final int[] l0 = new int[2];
        src.getLocationOnScreen(l0);

        final int[] l1 = new int[2];
        target.getLocationOnScreen(l1);

        l1[0] = l1[0] - l0[0] + target.getWidth() / 2;
        l1[1] = l1[1] - l0[1] + target.getHeight() / 2;

        return new Point(l1[0], l1[1]);
    }

    private int getColor(View view) {
        return Color.parseColor((String) view.getTag());
    }

    @Override
    public void onRefresh() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(false);

            }
        }, 2000);
    }

    @Override
    public void onLoad() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setLoading(false);

            }
        }, 1000);
    }

}
