package com.core.base.app.ui;

import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.core.base.R;
import com.core.base.widget.material.ButtonFlat;
import com.core.base.widget.material.ColorSelector;
import com.core.base.widget.material.Dialog;
import com.core.base.widget.material.ObservableScrollView;
import com.core.base.widget.material.ObservableScrollViewCallbacks;
import com.core.base.widget.material.ScrollState;
import com.core.base.widget.material.SnackBar;

public class MaterialActivity extends BaseActivity {

	private EditText etMaterial;

	public MaterialActivity() {
		super(R.layout.activity_material);

	}

	@Override
	public void initViews() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
		setSupportActionBar(toolbar);
		etMaterial = (EditText) findViewById(R.id.et_material);
	}

	@Override
	public void initData() {
		ObservableScrollView scrollView = (ObservableScrollView) findViewById(R.id.scroll);
		scrollView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {

			@Override
			public void onUpOrCancelMotionEvent(ScrollState scrollState) {
				ActionBar ab = getSupportActionBar();
				if (scrollState == ScrollState.UP) {
					if (ab.isShowing()) {
						ab.hide();
					}
				} else if (scrollState == ScrollState.DOWN) {
					if (!ab.isShowing()) {
						ab.show();
					}
				}

			}

			@Override
			public void onScrollChanged(int scrollY, boolean firstScroll,
					boolean dragging) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onDownMotionEvent() {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	public void bindViews() {
		// etMaterial.setTextColor(Color.BLACK);
		// SHOW SNACKBAR
		findViewById(R.id.buttonSnackBar).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(final View flatButton) {
						new SnackBar(
								MaterialActivity.this,
								"Do you want change color of this button to red?",
								"yes", new OnClickListener() {

									@Override
									public void onClick(View v) {
										ButtonFlat btn = (ButtonFlat) findViewById(R.id.buttonSnackBar);
										// btn.setTextColor(Color.RED);
									}
								}).show();
					}
				});
		// SHOW DiALOG
		findViewById(R.id.buttonDialog).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(final View flatButton) {
						Dialog dialog = new Dialog(
								MaterialActivity.this,
								"Title",
								"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam");
						dialog.setOnAcceptButtonClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								Toast.makeText(MaterialActivity.this,
										"Click accept button", 1).show();
							}
						});
						dialog.setOnCancelButtonClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								Toast.makeText(MaterialActivity.this,
										"Click cancel button", 1).show();
							}
						});
						dialog.show();
					}
				});
		// SHOW COLOR SEECTOR
		findViewById(R.id.buttonColorSelector).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(final View flatButton) {
						new ColorSelector(MaterialActivity.this, Color.RED,
								null).show();
					}
				});
	}

}
