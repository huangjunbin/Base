package com.core.base.util;

import android.util.Log;

public class LogUtil {

	public static void d(String TAG, String msg, boolean isLog) {
		if (isLog)
			Log.d(TAG, msg);
	}

	public static void e(String TAG, String msg, boolean isLog) {
		if (isLog)
			Log.e(TAG, msg);
	}
}
