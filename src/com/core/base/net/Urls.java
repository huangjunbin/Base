package com.core.base.net;

/**
 * 业务数据URL地址（注意标注地址注释）
 * 
 * @author allen
 * @version 1.0.0
 * @created 2014-7-10
 */
public class Urls {
	public static final boolean TEST_MODE = true;// 当前连接服务器模式，测试模式还是产线模式
	/** 默认的API头地址 */
	public static final String TEST_HEAD_URL = "http://121.40.155.118:8080/pupu/api/";
	public static final String ONLINE_HEAD_URL = "http://121.40.155.118:8080/pupu/api/";
	public static final String HEAD_URL = TEST_MODE ? TEST_HEAD_URL
			: ONLINE_HEAD_URL;
	public static final String TEST_IMAGE_URL = "http://121.40.155.118:8080/pupu/api/";
	public static final String ONLINE_IMAGE_URL = "http://121.40.155.118:8080/pupu/api/";
	public static final String IMAGE_URL = TEST_MODE ? TEST_IMAGE_URL
			: ONLINE_IMAGE_URL;
	// 示例
	public static final String example_action = HEAD_URL + "example.action";
	// 登陆
	public static final String login_action = HEAD_URL + "member_login.action";
	// 获取验证码
	public static final String getAutoCode_action = HEAD_URL
			+ "member_getAutoCode.action";
	// 校验验证码
	public static final String checkoutAutoCode_action = HEAD_URL
			+ "member_checkoutAutoCode.action";
	// 注册
	public static final String register_action = HEAD_URL
			+ "member_register.action";
	// 设置新密码
	public static final String setNewPassword_action = HEAD_URL
			+ "member_setNewPassword.action";
	// 更新个人信息
	public static final String updateMemberInfo_action = HEAD_URL
			+ "member_updateMemberInfo.action";
}
