package com.core.base.model;

/**
 * @className：ResponeModel.java
 * @author: allen
 * @Function: 网络响应模型
 * @createDate: 2014-8-29
 * @update:
 */
public class ResponeModel {
	private Object result;// 转换的Json
	private Object dataResult;//转化为对象的json
	private String json;// 服务器返回的整体Json
	private Integer code = 0;// 错误码
	private boolean status = false;// 状态
	private String msg = "";// 错误码
	private String data = "";// 数据
	private String totalCount;// 总条数
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public Object getDataResult() {
		return dataResult;
	}
	public void setDataResult(Object dataResult) {
		this.dataResult = dataResult;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}
	@Override
	public String toString() {
		return "ResponeModel [result=" + result + ", dataResult=" + dataResult
				+ ", json=" + json + ", code=" + code + ", status=" + status
				+ ", msg=" + msg + ", data=" + data + ", totalCount="
				+ totalCount + "]";
	}


}
