package com.core.base.db;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class DataBase {
	public final static String SHAPENAME = "roborder";

	// boolean数据
	public static void saveBoolean(Activity activity, String key, boolean value) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME,
				Context.MODE_PRIVATE);
		Editor edit = sp.edit();
		edit.putBoolean(key, value);
		edit.commit();
	}

	public static boolean getBoolean(Activity activity, String key) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME, 0);
		return sp.getBoolean(key, false);
	}

	// int数据
	public static void saveInt(Activity activity, String key, int value) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME,
				Context.MODE_PRIVATE);
		Editor edit = sp.edit();
		edit.putInt(key, value);
		edit.commit();
	}

	public static int getInt(Activity activity, String key) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME, 0);
		return sp.getInt(key, 0);
	}

	// long数据
	public static void saveLong(Activity activity, String key, long value) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME,
				Context.MODE_PRIVATE);
		Editor edit = sp.edit();
		edit.putLong(key, value);
		edit.commit();
	}

	public static long getLong(Activity activity, String key) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME, 0);
		return sp.getLong(key, 0);
	}

	// String数据
	public static void saveString(Activity activity, String key, String value) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME,
				Context.MODE_PRIVATE);
		Editor edit = sp.edit();
		edit.putString(key, value);
		edit.commit();
	}

	public static String getString(Activity activity, String key) {
		SharedPreferences sp = activity.getSharedPreferences(SHAPENAME, 0);
		return sp.getString(key, "");
	}
}
