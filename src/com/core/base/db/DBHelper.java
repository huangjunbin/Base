package com.core.base.db;

import android.content.Context;

import com.simple.util.db.operation.SimpleDbHelper;

public class DBHelper extends SimpleDbHelper {

	private static final String DBNAME = "4gvideo.db";
	private static final int DBVERSION = 1;
	private static final Class<?>[] clazz = {};

	public DBHelper(Context context) {
		super(context, DBNAME, null, DBVERSION, clazz);
	}
}